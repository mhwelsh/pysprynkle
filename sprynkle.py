#/usr/bin/python
import json
import urllib2
from datetime import date, timedelta
import argparse

def getLast3DayTotalPrecip(d=date.today()):
    t = 0.0
    for i in range(-3,0):
        url = 'http://api.wunderground.com/api/72ab7207937647ed/history_' + (d + timedelta(i)).strftime('%Y%m%d') + '/q/CA/Goleta.json'
        precipm = json.load(urllib2.urlopen(url))['history']['dailysummary'][0]['precipm']
        t = t + float(precipm)
    return t

#Control zone at lowest level (Not Implemented Completely)
def I2CInterfaceIO(pin, value):
    print "Pin "+str(pin)+" set to "+str(value)


    parser = argparse.ArgumentParser(description='Control Sprinklers')
    parser.add_argument('zone',type=int, help='Sprinkler Zone to contol')
    parser.add_argument('command', type=str, help='Sprinkler Command, on or off')
    parser.add_argument('-t', type=float, dest='threshold', help='Threshold in mm for sprinkler to operate')
    
    args = parser.parse_args()
    
    
    if args.command=='on':
        if threshold:
            if threshold > getLast3DayPrecip():
                I2CInterfaceIO(args.zone-1,1)
        else:
            I2CInterfaceIO(args.zone-1,1)
            
    elif args.command=='off':
        I2CInterfaceIO(args.zone-1,0)
        

